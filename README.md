# sshd

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with sshd](#setup)
    * [What sshd affects](#what-sshd-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with sshd](#beginning-with-sshd)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview
Manages the configuration of /etc/sshd_config using an erb template.

## Module Description
Manages the configuration of /etc/sshd_config using an erb template.


### What sshd affects

 /etc/sshd_config

### Beginning with sshd

The very basic steps needed for a user to get the module up and running.


## Usage

 Sample Usage:

  class { 'ssh':
      port => '22',
  }

## Reference

## Limitations

Tested on CentOS / RHEL 6/7

## Development

## Release Notes/Contributors/Etc **Optional**

None
