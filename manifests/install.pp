# ssh install
class ssh::install () {
  package { "$ssh::pkg_name":
    ensure => $ssh::pkg_ensure,
    name   => $ssh::pkg_name,
  }
}
