class ssh::service () {
  service { "$ssh::service_name":
    ensure     => $ssh::service_ensure,
    enable     => $ssh::service_enabled,
    hasstatus  => true,
    hasrestart => true,
    require    => Class['ssh::install'],
  }
  file { "$ssh::service_configfile":
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('ssh/sshd_config.erb'),
    notify  => Service["$ssh::service_name"]
  }
}
